# Instalar Apache Tomcat 8.5 sobre Ubuntu 16.04 Server

## Introducción

Apache Tomcat es un servidor web y contenedor de servlet que se usa para servir aplicaciones Java. Tomcat es una implementación open source de las tecnologías Java Servlet y JavaServer Pages, desarrollada por Apache Software Foundation.

En esta práctica veremos cómo realizar la instalación de un servidor Tomcat 8.5 sobre Ubuntu 16.04 Server.

## Prerequisitos

Para poder llevar a cabo esta práctica debemos tener instalada una máquina virtual con Ubuntu 16.04 Server. 

Si no tenemos instalada una máquina virtual podemos descargar la iso de [esta URL]](http://releases.ubuntu.com/16.04.3/ubuntu-16.04.3-server-amd64.iso)


## Instalación de las Oracle JDK 8

Para instalar la versión oficial de JDK distribuida por Oracle, debemos agregar el PPA de Oracle, y, posteriormente, actualizar el repositorio de paquetes:

```bash
$ sudo add-apt-repository ppa:webupd8team/java
$ sudo apt-get update
$ sudo apt-get install oracle-java8-installer
```

## Configurando las JDK de Oracle

Puede darse el caso de que existan varias versiones de Java instaladas en el servidor. Para ello, configuramos cuál será la versión de java que usaremos por defecto mediante el comando _update-alternatives_:

```bash
$ sudo update-alternatives --config java
Existe 1 opción para la alternativa java (que provee /usr/bin/java).

  Selección   Ruta                                     Prioridad  Estado
------------------------------------------------------------
  0            /usr/lib/jvm/java-8-oracle/jre/bin/java   1081      modo automático
* 1            /usr/lib/jvm/java-8-oracle/jre/bin/java   1081      modo manual

Press <enter> to keep the current choice[*], or type selection number: 1
```

Elegimos el número que corresponde a la versión de java que queremos usar como predeterminada. Esto también se puede hacer para otros comandos Java, como el compilador (javac), el generador de documentación(javadoc), la herramienta JAR de firma (jarsigner), ...

```bash
$ java -version
java version "1.8.0_151"
Java(TM) SE Runtime Environment (build 1.8.0_151-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.151-b12, mixed mode)
```


## Definiendo la Variable de Entorno JAVA_HOMe

Muchos programas basados en Java, como puede ser Tomcat, Jetty, Glassfish, Cassandra o Jenkins, usan la variable de entorno __ JAVA_HOME__ para determinar la ubicación de la instalación de Java. Para establecer esta variable de entorno, primero debemos averiguar dónde está instalado Java. Para ello podemos hacer uso del comando ejecutado en el apartado anterior:

```bash
$ sudo update-alternatives --config java
```

Copiamos la ruta de la instalación que hayamos elegido y abrimos el fichero _/etc/environment_ con un editor de texto:

```bash
$ sudo nano /etc/environment
```

Alñ final del archivo, agregamos la siguiente línea con la ruta de la instalación de Java que hayamos elegido::

```bash
JAVA_HOME=/usr/lib/jvm/java-8-oracle
```

Guardamos el fichero y lo volvemos a cargar:

```bash
$ source /etc/environment
```


Ahora podemos probar si la variable de entorno se ha establecido mediante la ejecutación del siguiente comando:

```bash
$ echo $JAVA_HOME
```

## Crear el Usuario Tomcat

Por temas de seguridad, el proceso del Tomcat debería ejecutarlo un usuario que no tenga privilegios de root. Para este propósito, crearemos el usuario _tomcat_ en el sistema.

Primero, creamos el grupo _tomcat_:

```bash
$ sudo groupadd tomcat
```

Segundo, creamos el usuario _tomcat_. Crearemos el usuario como miembro del grupo _tomcat_, con una carpeta HOME _/opt/tomcat_ (será el directorio de instalación del tomcat), y con la shell _/bin/false_ (el usuario tomcat será un usuario de servicio y no podrá hacer login):

```bash
sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
```

## Instalar Tomcat 8.5

La mejor manera de instalar Tomcat 8.5 es descargar la última versión binaria de la la página oficial y configurarlo manualmente.

Encontar la última versión de Tomcat 8.5 en la [Página de descargas de Tomcat](http://tomcat.apache.org/download-80.cgi). En el momento de escribir este tutorial, la última versión de Tomcat era la [8.5.23](http://ftp.cixug.es/apache/tomcat/tomcat-8/v8.5.23/bin/apache-tomcat-8.5.23.tar.gz). Bajo la sessión __Binary Distributions__, bajo la lista __Core__,  y copiar el link con extensión __tar.gz__. Una vez copiado, lo descargamos al directorio temporal del servidor:

```bash
$ cd /tmp
$ curl -O http://ftp.cixug.es/apache/tomcat/tomcat-8/v8.5.23/bin/apache-tomcat-8.5.23.tar.gz
```

Una vez descargado, instalaremos el Tomcat en el directorio _/opt/tomcat_. Creamos este directorio y extramos el contenido del fichero descargado dentro:

```bash
$ sudo mkdir /opt/tomcat
$ sudo tar xzvf /tmp/apache-tomcat-8.5.23.tar.gz -C /opt/tomcat --strip-components=1
```

## Actualizar Permisos del Sistema

El usuario _tomcat_ necesita tener permisos de acceso al directorio de instalación del Tomcat.

Para ello hacemos que el propietario de la carpeta de instalación del Tomcat sea el usuario tomcat:

```bash
$ sudo chown -R tomcat:tomcat /opt/tomcat
```

Comprobamos que ahora el propietario del directorio _/opt/tomcat_ es el usuario _tomcat_:

```bash
$ ls -l /opt/tomcat/
total 112
drwxr-x--- 2 tomcat tomcat  4096 oct 31 10:10 bin
drwx------ 2 tomcat tomcat  4096 sep 28 11:31 conf
drwxr-x--- 2 tomcat tomcat  4096 oct 31 10:10 lib
-rw-r----- 1 tomcat tomcat 57092 sep 28 11:31 LICENSE
drwxr-x--- 2 tomcat tomcat  4096 sep 28 11:30 logs
-rw-r----- 1 tomcat tomcat  1723 sep 28 11:31 NOTICE
-rw-r----- 1 tomcat tomcat  7064 sep 28 11:31 RELEASE-NOTES
-rw-r----- 1 tomcat tomcat 15946 sep 28 11:31 RUNNING.txt
drwxr-x--- 2 tomcat tomcat  4096 oct 31 10:10 temp
drwxr-x--- 7 tomcat tomcat  4096 sep 28 11:30 webapps
drwxr-x--- 2 tomcat tomcat  4096 sep 28 11:30 work
```

## Crear el Fichero de Servicio Systemd

Si queremos ejecutar Tomcat como un servicio del sistema, debemos crear el fichero del servicio systemd.

Tomcat necesita conocer dónde están las Java instaladas. Este path se encuentra definido en la variable del sistema __JAVA_HOME__.

Se puede dar el caso de queramos que se lance el servicio del Tomcat con una Java que no es la que está definida en la variable del sistema __JAVA_HOME__, es por ello que incluiremos la definición de dicha variable dentro del fichero del servicio.

Los ficheros de servicios se definen en la carpeta __/etc/systemd/system/__, Creamos el fichero __tomcat.service__ dentro de la carpeta anterior:

```bash
$ sudo nano /etc/systemd/system/tomcat.service
```

Copia el siguiente código dentro del fichero:

```bash
[Unit]
Description=Apache Tomcat Web Application Container
After=network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/java-8-oracle
Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/opt/tomcat
Environment=CATALINA_BASE=/opt/tomcat
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/opt/tomcat/bin/startup.sh
ExecStop=/opt/tomcat/bin/shutdown.sh

User=tomcat
Group=tomcat
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
```

Tenemos que tener las siguientes consideraciones:
* __JAVA_HOME__: Debe apuntar a una instalación de java existente en nuestro servidor (En este caso /usr/lib/jvm/java-8-oracle).
* __CATALINA_HOME__: Debe apuntar al directorio de instalación del Tomcat (en este caso _/opt/tomcat_).
* __CATALINA_OPTS__: En esta variable podemos modificar los parámetros de asignación de memoria:
    * __Xms__: Memoria inicial que asignada a la Java Virtual Machine (JVM).
    * __Xmx__: Memoria máxima a la que puede llegar la Java Virtual Machine (JVM).


> __Nota__: Tal y como está configurado nuestro Tomcat, cuando arranque el servicio ocupará 512MB en memoria RAMy podrá crecer hasta un máximo de 1024MB.


Una vez creado el fichero , guardamos los cambios y cerramos el fichero.

Hacemos un reload del servicio systemd para que coja los cambios:

```bash
$ sudo systemctl daemon-reload
```

Arrancamos el sevicio del tomcat:

```bash
$ sudo systemctl start tomcat
```

Comprobamos que no ha habido errores durante el arranque del sevicio:

```bash
$ sudo systemctl status tomcat
```

La salida deberá ser algo parecido a esto:

```bash
● tomcat.service - Apache Tomcat Web Application Container
   Loaded: loaded (/etc/systemd/system/tomcat.service; disabled; vendor preset: enabled)
   Active: active (running) since mar 2017-10-31 10:45:07 WET; 3s ago
  Process: 3910 ExecStart=/opt/tomcat/bin/startup.sh (code=exited, status=0/SUCCESS)
 Main PID: 3920 (java)
    Tasks: 18
   Memory: 118.9M
      CPU: 2.754s
   CGroup: /system.slice/tomcat.service
           └─3920 /usr/lib/jvm/java-8-oracle/bin/java -Djava.util.logging.config.file=/opt/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djava.awt.headless=true -Djava.security.egd=file

oct 31 10:45:07 ubuntu systemd[1]: Starting Apache Tomcat Web Application Container...
oct 31 10:45:07 ubuntu startup.sh[3910]: Using CATALINA_BASE:   /opt/tomcat
oct 31 10:45:07 ubuntu startup.sh[3910]: Using CATALINA_HOME:   /opt/tomcat
oct 31 10:45:07 ubuntu startup.sh[3910]: Using CATALINA_TMPDIR: /opt/tomcat/temp
oct 31 10:45:07 ubuntu startup.sh[3910]: Using JRE_HOME:        /usr/lib/jvm/java-8-oracle
oct 31 10:45:07 ubuntu startup.sh[3910]: Using CLASSPATH:       /opt/tomcat/bin/bootstrap.jar:/opt/tomcat/bin/tomcat-juli.jar
oct 31 10:45:07 ubuntu startup.sh[3910]: Using CATALINA_PID:    /opt/tomcat/temp/tomcat.pid
oct 31 10:45:07 ubuntu startup.sh[3910]: Tomcat started.
oct 31 10:45:07 ubuntu systemd[1]: Started Apache Tomcat Web Application Container.
```

## Desplegar la Aplicación Web dummy.war en el Tomcat

Paramos el servidor Tomcat:

```bash
$ sudo systemctl stop tomcat
```

Nos traemos el fichero dummy.war que se encuentra en el repositorio git a nuestra máquina (Podemos hacer uso del comando _wget_ para descargar el archivo, o utilizar algún cliente que soporte SFTP como [Filezilla Client](https://filezilla-project.org/download.php) o [WinSCP](https://winscp.net/eng/download.php)).

Copiamos la aplicación descargada previamente en el directorio _/tmp_ en la carpeta _webapps_ dentro del directorio donde está instalado tomcat _/opt/tomcat_:

```bash
$ sudo cp /tmp/dummy.war /opt/tomcat/webapps/
```

Cambiamos el propietario del fichero _/opt/tomcat/webapps_ al usuario _tomcat_:

```bash
$ sudo chown tomcat:tomcat /opt/tomcat/webapps/dummy.war
```

Arrancamos el servicio del Tomcat:

```bash
$ sudo systemctl start tomcat
```

## Comprobar si ha Desplegado la Aplicación dummy

Si se ha desplegado correctamente la aplicación deberíamos ver la siguiente línea en el log del tomcat:

```bash
$ sudo less /opt/tomcat/logs/catalina.out
...
31-Oct-2017 11:42:15.165 INFORMACIÓN [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployWAR Despliegue del archivo [/opt/tomcat/webapps/dummy.war] de la aplicación web
31-Oct-2017 11:42:15.887 INFORMACIÓN [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployWAR Deployment of web application archive [/opt/tomcat/webapps/dummy.war] has finished in [724] ms
```

El Tomcat escucha por defecto en el puerto _8080_, así que nuestra aplicación será accesible a través de la URL _http://localhost:8080/dummy/index.jsp_:

El comando curl nos permite hacer llamadas a URLs a través de línea de comandos, para instalar curl en nuestro servidor:

```bash
$ sudo apt-get install curl
```

El siguiente comando invoca una URL y muestra el código que devuelve el servidor Tomcat, si devuelve un 200 es que el recurso (en este caso la aplicación dummy) se encuentra disponible:

```bash
$ curl -I http://localhost:8080/dummy/index.jsp
HTTP/1.1 200
Set-Cookie: JSESSIONID=B81A0022F0F92772B44D4A5372C751EA; Path=/dummy; HttpOnly
Content-Type: text/html;charset=ISO-8859-1
Transfer-Encoding: chunked
Date: Tue, 31 Oct 2017 11:52:43 GMT
```

Podemos ver el contenido de la URL invocada:

```bash
$ curl -L http://localhost:8080/dummy/index.jsp

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PSP</title>
</head>
<body>
Welcome to PSP Module
</body>
</html>
```

## Identificar el Proceso del Sistema del Tomcat

El comando __ps__ en linux nos permite ver los procesos que se están ejecutando actualmente en nuestro sistema.

Para ver el proceso del Tomcat ejecutamos el siguiente comando:

```bash
jonay@ubuntu:~$ ps -fea
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 09:36 ?        00:00:01 /sbin/init
...
tomcat    4217     1  0 10:45 ?        00:00:09 /usr/lib/jvm/java-8-oracle/bin/java -Djava.util.logging.config.file=/opt/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djava.awt.headless
```

En este caso tenemos en cuenta los siguientes aspectos:
* __UID__: Propietario del proceso (en este caso el usuario _tomcat_).
* __PID__: Número del proceso (en este caso es 4217). Este número es distinto cada vez que se arranca el servicio del Tomcat.


Otra manera de indentificar los procesos Java que se están ejecutando en nuestro sistema con el comando __jps__:

```bash
jonay@ubuntu:~/jprocps$ sudo jps -l
4613 sun.tools.jps.Jps
4217 org.apache.catalina.startup.Bootstrap
```

La salida que hace referencia a _org.apache.catalina.startup.Bootstrap_ corresponde al proceso del Tomcat, en este caso sería el PID 4217.

El comando __jstack__ nos permite ver los Threads que está ejecutando actualmente un proceso Java en nuestro sistema (hay que pasarle como parámetro el número del PID del proceso, en este caso es el PID 4217):

```bash
$ sudo -u tomcat jstack 4217
2017-10-31 12:14:15
Full thread dump Java HotSpot(TM) 64-Bit Server VM (25.151-b12 mixed mode):

"Attach Listener" #44 daemon prio=9 os_prio=0 tid=0x00007f860c003800 nid=0x11d5 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"ajp-nio-8009-AsyncTimeout" #42 daemon prio=5 os_prio=0 tid=0x00007f86384bd000 nid=0x10a4 waiting on condition [0x00007f860bbfa000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
        at java.lang.Thread.sleep(Native Method)
        at org.apache.coyote.AbstractProtocol$AsyncTimeout.run(AbstractProtocol.java:1211)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-Acceptor-0" #41 daemon prio=5 os_prio=0 tid=0x00007f86384bb000 nid=0x10a3 runnable [0x00007f860bcfb000]
   java.lang.Thread.State: RUNNABLE
        at sun.nio.ch.ServerSocketChannelImpl.accept0(Native Method)
        at sun.nio.ch.ServerSocketChannelImpl.accept(ServerSocketChannelImpl.java:422)
        at sun.nio.ch.ServerSocketChannelImpl.accept(ServerSocketChannelImpl.java:250)
        - locked <0x00000000c02a3908> (a java.lang.Object)
        at org.apache.tomcat.util.net.NioEndpoint$Acceptor.run(NioEndpoint.java:455)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-ClientPoller-0" #40 daemon prio=5 os_prio=0 tid=0x00007f86384b9800 nid=0x10a2 runnable [0x00007f860bdfc000]
   java.lang.Thread.State: RUNNABLE
        at sun.nio.ch.EPollArrayWrapper.epollWait(Native Method)
        at sun.nio.ch.EPollArrayWrapper.poll(EPollArrayWrapper.java:269)
        at sun.nio.ch.EPollSelectorImpl.doSelect(EPollSelectorImpl.java:93)
        at sun.nio.ch.SelectorImpl.lockAndDoSelect(SelectorImpl.java:86)
        - locked <0x00000000c02a40b0> (a sun.nio.ch.Util$3)
        - locked <0x00000000c02a40a0> (a java.util.Collections$UnmodifiableSet)
        - locked <0x00000000c02a3f68> (a sun.nio.ch.EPollSelectorImpl)
        at sun.nio.ch.SelectorImpl.select(SelectorImpl.java:97)
        at org.apache.tomcat.util.net.NioEndpoint$Poller.run(NioEndpoint.java:793)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-10" #39 daemon prio=5 os_prio=0 tid=0x00007f86384aa800 nid=0x10a1 waiting on condition [0x00007f860befd000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-9" #38 daemon prio=5 os_prio=0 tid=0x00007f86384a8800 nid=0x10a0 waiting on condition [0x00007f860bffe000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-8" #37 daemon prio=5 os_prio=0 tid=0x00007f86384a7000 nid=0x109f waiting on condition [0x00007f86201ff000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-7" #36 daemon prio=5 os_prio=0 tid=0x00007f86384a5000 nid=0x109e waiting on condition [0x00007f8620300000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-6" #35 daemon prio=5 os_prio=0 tid=0x00007f86384a3000 nid=0x109d waiting on condition [0x00007f8620401000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-5" #34 daemon prio=5 os_prio=0 tid=0x00007f86384a1000 nid=0x109c waiting on condition [0x00007f8620502000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-4" #33 daemon prio=5 os_prio=0 tid=0x00007f863849f000 nid=0x109b waiting on condition [0x00007f8620603000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-3" #32 daemon prio=5 os_prio=0 tid=0x00007f863849d000 nid=0x109a waiting on condition [0x00007f8620704000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-2" #31 daemon prio=5 os_prio=0 tid=0x00007f863849b000 nid=0x1099 waiting on condition [0x00007f8620805000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ajp-nio-8009-exec-1" #30 daemon prio=5 os_prio=0 tid=0x00007f8638499000 nid=0x1098 waiting on condition [0x00007f8620906000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c029f738> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-AsyncTimeout" #29 daemon prio=5 os_prio=0 tid=0x00007f8638497000 nid=0x1097 waiting on condition [0x00007f8620a07000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
        at java.lang.Thread.sleep(Native Method)
        at org.apache.coyote.AbstractProtocol$AsyncTimeout.run(AbstractProtocol.java:1211)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-Acceptor-0" #28 daemon prio=5 os_prio=0 tid=0x00007f8638495800 nid=0x1096 runnable [0x00007f8620b08000]
   java.lang.Thread.State: RUNNABLE
        at sun.nio.ch.ServerSocketChannelImpl.accept0(Native Method)
        at sun.nio.ch.ServerSocketChannelImpl.accept(ServerSocketChannelImpl.java:422)
        at sun.nio.ch.ServerSocketChannelImpl.accept(ServerSocketChannelImpl.java:250)
        - locked <0x00000000c030ab30> (a java.lang.Object)
        at org.apache.tomcat.util.net.NioEndpoint$Acceptor.run(NioEndpoint.java:455)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-ClientPoller-0" #27 daemon prio=5 os_prio=0 tid=0x00007f8638494000 nid=0x1095 runnable [0x00007f8620c09000]
   java.lang.Thread.State: RUNNABLE
        at sun.nio.ch.EPollArrayWrapper.epollWait(Native Method)
        at sun.nio.ch.EPollArrayWrapper.poll(EPollArrayWrapper.java:269)
        at sun.nio.ch.EPollSelectorImpl.doSelect(EPollSelectorImpl.java:93)
        at sun.nio.ch.SelectorImpl.lockAndDoSelect(SelectorImpl.java:86)
        - locked <0x00000000c02fc458> (a sun.nio.ch.Util$3)
        - locked <0x00000000c02fc448> (a java.util.Collections$UnmodifiableSet)
        - locked <0x00000000c02fc1e0> (a sun.nio.ch.EPollSelectorImpl)
        at sun.nio.ch.SelectorImpl.select(SelectorImpl.java:97)
        at org.apache.tomcat.util.net.NioEndpoint$Poller.run(NioEndpoint.java:793)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-10" #26 daemon prio=5 os_prio=0 tid=0x00007f8638483800 nid=0x1094 waiting on condition [0x00007f8620d0a000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-9" #25 daemon prio=5 os_prio=0 tid=0x00007f8638481800 nid=0x1093 waiting on condition [0x00007f8620e0b000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-8" #24 daemon prio=5 os_prio=0 tid=0x00007f8638480000 nid=0x1092 waiting on condition [0x00007f8620f0c000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-7" #23 daemon prio=5 os_prio=0 tid=0x00007f863847e000 nid=0x1091 waiting on condition [0x00007f862100d000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-6" #22 daemon prio=5 os_prio=0 tid=0x00007f863847c000 nid=0x1090 waiting on condition [0x00007f862110e000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-5" #21 daemon prio=5 os_prio=0 tid=0x00007f863847a000 nid=0x108f waiting on condition [0x00007f862120f000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-4" #20 daemon prio=5 os_prio=0 tid=0x00007f8638478000 nid=0x108e waiting on condition [0x00007f8621310000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-3" #19 daemon prio=5 os_prio=0 tid=0x00007f8638476000 nid=0x108d waiting on condition [0x00007f8621411000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-2" #18 daemon prio=5 os_prio=0 tid=0x00007f8638474000 nid=0x108c waiting on condition [0x00007f8621512000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"http-nio-8080-exec-1" #17 daemon prio=5 os_prio=0 tid=0x00007f8638472800 nid=0x108b waiting on condition [0x00007f8621613000]
   java.lang.Thread.State: WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c02a62a8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
        at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:103)
        at org.apache.tomcat.util.threads.TaskQueue.take(TaskQueue.java:31)
        at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
        at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
        at java.lang.Thread.run(Thread.java:748)

"ContainerBackgroundProcessor[StandardEngine[Catalina]]" #16 daemon prio=5 os_prio=0 tid=0x00007f8638470000 nid=0x108a waiting on condition [0x00007f8621714000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
        at java.lang.Thread.sleep(Native Method)
        at org.apache.catalina.core.ContainerBase$ContainerBackgroundProcessor.run(ContainerBase.java:1355)
        at java.lang.Thread.run(Thread.java:748)

"NioBlockingSelector.BlockPoller-2" #13 daemon prio=5 os_prio=0 tid=0x00007f863845d800 nid=0x1087 runnable [0x00007f8621e62000]
   java.lang.Thread.State: RUNNABLE
        at sun.nio.ch.EPollArrayWrapper.epollWait(Native Method)
        at sun.nio.ch.EPollArrayWrapper.poll(EPollArrayWrapper.java:269)
        at sun.nio.ch.EPollSelectorImpl.doSelect(EPollSelectorImpl.java:93)
        at sun.nio.ch.SelectorImpl.lockAndDoSelect(SelectorImpl.java:86)
        - locked <0x00000000c02a20e8> (a sun.nio.ch.Util$3)
        - locked <0x00000000c02a20d8> (a java.util.Collections$UnmodifiableSet)
        - locked <0x00000000c02a1fa0> (a sun.nio.ch.EPollSelectorImpl)
        at sun.nio.ch.SelectorImpl.select(SelectorImpl.java:97)
        at org.apache.tomcat.util.net.NioBlockingSelector$BlockPoller.run(NioBlockingSelector.java:339)

"NioBlockingSelector.BlockPoller-1" #12 daemon prio=5 os_prio=0 tid=0x00007f863844e800 nid=0x1086 runnable [0x00007f8621f63000]
   java.lang.Thread.State: RUNNABLE
        at sun.nio.ch.EPollArrayWrapper.epollWait(Native Method)
        at sun.nio.ch.EPollArrayWrapper.poll(EPollArrayWrapper.java:269)
        at sun.nio.ch.EPollSelectorImpl.doSelect(EPollSelectorImpl.java:93)
        at sun.nio.ch.SelectorImpl.lockAndDoSelect(SelectorImpl.java:86)
        - locked <0x00000000c0302328> (a sun.nio.ch.Util$3)
        - locked <0x00000000c0302318> (a java.util.Collections$UnmodifiableSet)
        - locked <0x00000000c03021f0> (a sun.nio.ch.EPollSelectorImpl)
        at sun.nio.ch.SelectorImpl.select(SelectorImpl.java:97)
        at org.apache.tomcat.util.net.NioBlockingSelector$BlockPoller.run(NioBlockingSelector.java:339)

"GC Daemon" #11 daemon prio=2 os_prio=0 tid=0x00007f86383a7000 nid=0x1085 in Object.wait() [0x00007f86226d1000]
   java.lang.Thread.State: TIMED_WAITING (on object monitor)
        at java.lang.Object.wait(Native Method)
        - waiting on <0x00000000c06b8bf8> (a sun.misc.GC$LatencyLock)
        at sun.misc.GC$Daemon.run(GC.java:117)
        - locked <0x00000000c06b8bf8> (a sun.misc.GC$LatencyLock)

"AsyncFileHandlerWriter-1300109446" #10 daemon prio=5 os_prio=0 tid=0x00007f863811e800 nid=0x1084 waiting on condition [0x00007f8623087000]
   java.lang.Thread.State: TIMED_WAITING (parking)
        at sun.misc.Unsafe.park(Native Method)
        - parking to wait for  <0x00000000c0107060> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
        at java.util.concurrent.locks.LockSupport.parkNanos(LockSupport.java:215)
        at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2078)
        at java.util.concurrent.LinkedBlockingDeque.pollFirst(LinkedBlockingDeque.java:522)
        at java.util.concurrent.LinkedBlockingDeque.poll(LinkedBlockingDeque.java:684)
        at org.apache.juli.AsyncFileHandler$LoggerThread.run(AsyncFileHandler.java:160)

"Service Thread" #7 daemon prio=9 os_prio=0 tid=0x00007f86380b8800 nid=0x1082 runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"C1 CompilerThread1" #6 daemon prio=9 os_prio=0 tid=0x00007f86380b5800 nid=0x1081 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"C2 CompilerThread0" #5 daemon prio=9 os_prio=0 tid=0x00007f86380b3000 nid=0x1080 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"Signal Dispatcher" #4 daemon prio=9 os_prio=0 tid=0x00007f86380b1800 nid=0x107f runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"Finalizer" #3 daemon prio=8 os_prio=0 tid=0x00007f863807e000 nid=0x107e in Object.wait() [0x00007f8623bd9000]
   java.lang.Thread.State: WAITING (on object monitor)
        at java.lang.Object.wait(Native Method)
        - waiting on <0x00000000c06b9f08> (a java.lang.ref.ReferenceQueue$Lock)
        at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:143)
        - locked <0x00000000c06b9f08> (a java.lang.ref.ReferenceQueue$Lock)
        at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:164)
        at java.lang.ref.Finalizer$FinalizerThread.run(Finalizer.java:209)

"Reference Handler" #2 daemon prio=10 os_prio=0 tid=0x00007f8638079800 nid=0x107d in Object.wait() [0x00007f8623cda000]
   java.lang.Thread.State: WAITING (on object monitor)
        at java.lang.Object.wait(Native Method)
        - waiting on <0x00000000c06b9f38> (a java.lang.ref.Reference$Lock)
        at java.lang.Object.wait(Object.java:502)
        at java.lang.ref.Reference.tryHandlePending(Reference.java:191)
        - locked <0x00000000c06b9f38> (a java.lang.ref.Reference$Lock)
        at java.lang.ref.Reference$ReferenceHandler.run(Reference.java:153)

"main" #1 prio=5 os_prio=0 tid=0x00007f863800a000 nid=0x107a runnable [0x00007f8641857000]
   java.lang.Thread.State: RUNNABLE
        at java.net.PlainSocketImpl.socketAccept(Native Method)
        at java.net.AbstractPlainSocketImpl.accept(AbstractPlainSocketImpl.java:409)
        at java.net.ServerSocket.implAccept(ServerSocket.java:545)
        at java.net.ServerSocket.accept(ServerSocket.java:513)
        at org.apache.catalina.core.StandardServer.await(StandardServer.java:466)
        at org.apache.catalina.startup.Catalina.await(Catalina.java:758)
        at org.apache.catalina.startup.Catalina.start(Catalina.java:704)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)
        at org.apache.catalina.startup.Bootstrap.start(Bootstrap.java:355)
        at org.apache.catalina.startup.Bootstrap.main(Bootstrap.java:495)

"VM Thread" os_prio=0 tid=0x00007f8638072000 nid=0x107c runnable

"GC task thread#0 (ParallelGC)" os_prio=0 tid=0x00007f863801f800 nid=0x107b runnable

"VM Periodic Task Thread" os_prio=0 tid=0x00007f86380bb800 nid=0x1083 waiting on condition

JNI global references: 235

```